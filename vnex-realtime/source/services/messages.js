const messages = [];

const userLogin = [];

const get_message_room = (room) => {
  return messages.filter((message) => {
    const messageDefine = [
      message.type + "#" + message.from,
      message.type + "#" + message.to,
    ].join(":");
    const roomDefine = [
      room.type + "#" + room.from,
      room.type + "#" + room.to,
    ].join(":");
    const roomDefineReverse = [
      room.type + "#" + room.to,
      room.type + "#" + room.from,
    ].join(":");
    return messageDefine === roomDefine || messageDefine === roomDefineReverse;
  });
};

module.exports = (socket, io) => {
  socket.emit("user-online-list", userLogin);

  socket.on("user-login", (userData) => {
    userData.type = "ACCOUNT";
    const isLogin = userLogin.find((u) => u.username === userData.username);
    if (!isLogin) userLogin.push(userData);
    io.emit("user-online-list", userLogin);
  });

  socket.on("user-logout", (userData) => {
    const index = userLogin.findIndex((u) => u.username === userData.username);
    userLogin.splice(index, 1);
    io.emit("user-online-list", userLogin);
  });

  socket.on("join-room", (room) => {
    socket.join(room.type + "#" + room.to);
    socket.emit("history", get_message_room(room));
  });

  socket.on("message", (message) => {
    messages.push(message);
    const caseDefine = [
      message.type + "#" + message.from,
      message.type + "#" + message.to,
    ];
    const caseDefineReverse = [
      message.type + "#" + message.to,
      message.type + "#" + message.from,
    ];
    message.code = [caseDefine.join(":"), caseDefineReverse.join(":")];
    socket.nsp.to(caseDefine).emit("message", message);
  });
};
