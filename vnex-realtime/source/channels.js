const messages = require("./services/messages");

module.exports = (socket, io) => {
  messages(socket, io);
};
