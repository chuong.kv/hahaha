const http = require("http");
const express = require("express");
const helmet = require("helmet");
const xss = require("xss-clean");
const compression = require("compression");
const cors = require("cors");
const path = require("path");
const httpStatus = require("http-status");
const config = require("./configs/config");
const morgan = require("./configs/morgan");
const { errorConverter, errorHandler } = require("./middlewares/error");
const ApiError = require("./utils/ApiError");
const socketio = require("socket.io");
const channels = require("./channels");

const app = express();

if (config.env !== "test") {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}

// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());

// gzip compression
app.use(compression());

const corsOptions = {
  origin: "*",
  methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"],
};

// enable cors
app.use(cors(corsOptions));
app.options("*", cors(corsOptions));

app.use(express.static(path.resolve(path.join(__dirname, "..", "public"))));

// send back a 404 error for any unknown api request
app.use((req, res, next) => {
  next(new ApiError(httpStatus.NOT_FOUND, "Not found"));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

const server = http.createServer(app);
const socket = socketio(server, {
  cors: {
    origin: "*",
  },
});

socket.on("connection", (ioSocket) => {
  channels(ioSocket, socket);
});

module.exports = server;
